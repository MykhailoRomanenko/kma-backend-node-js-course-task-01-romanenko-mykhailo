require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const Service = require("./simple-service")
const errorHandler = require('./error-handler')
const createHttpError = require('http-errors')

const app = express()
app.use(bodyParser.text())
app.use(bodyParser.json())

app.post('/square', (req, res, next) => {
    const n = parseFloat(req.body)
    if (n === undefined) {
        return next(createHttpError(400, 'Please provide a valid number'))
    }
    return res.json({
        number: n,
        square: n * n
    })
})

app.post('/reverse', (req, res) => {
    return res.send(Service.reverseString(req.body))
})

app.get('/date/:year/:month/:day', (req, res, next) => {
    const {year, month, day} = req.params
    const date = new Date(year, month - 1, day)
    if (!date) {
        return next(createHttpError(400, 'Year, month or day is/are not provided correctly'))
    }
    return res.json(Service.getDateInformation(date))
})

app.use(errorHandler);

const port = process.env.PORT || '56201'
app.listen(port, () => {
    console.log(`Server listening at port ${port}`)
})

const dummy = 0