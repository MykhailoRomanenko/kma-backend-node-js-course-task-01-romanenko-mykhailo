const createHttpError = require('http-errors')
const weekDays = require("./week-days");

function getDateInformation(date) {
    console.log(date.toLocaleString())
    if (date.toString() === 'Invalid Date') {
        throw createHttpError(400, 'Please provide a valid date')
    }
    return {
        weekDay: weekDays[date.getDay()],
        isLeapYear: isLeapYear(date.getFullYear()),
        difference: dateDiffInDays(date, new Date())
    }
}

function isLeapYear(y) {
    if (y % 4 !== 0) {
        return false
    }
    if (y % 100 !== 0) {
        return true
    }
    return y % 400 === 0;
}

const MS_IN_DAY = 1000 * 60 * 60 * 24;

function dateDiffInDays(a, b) {
    const utc1 = BigInt(Date.UTC(a.getFullYear(), a.getMonth(), a.getDate()))
    const utc2 = BigInt(Date.UTC(b.getFullYear(), b.getMonth(), b.getDate()))

    return Math.abs(Number((utc1 - utc2) / BigInt(MS_IN_DAY)));
}


function reverseString(str) {
    return Array.from(str).reverse().join('')
}

module.exports = {getDateInformation, reverseString}